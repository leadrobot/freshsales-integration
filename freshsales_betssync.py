import freshsaleshelper
from sqlalchemy import create_engine, MetaData

postgres_engine = create_engine('postgresql://postgres:BIcA1NM4mvAAJljE@35.225.19.27:5432/betxchange')
meta = MetaData(postgres_engine)
meta.reflect()

bets_table = meta.tables['bets']
bet_lines_table = meta.tables['bet_lines']

def sync():
    freshsaleshelper.Integrator().insert_bets(postgres_engine, bets_table, bet_lines_table)


if __name__ == '__main__':
	while True:
		sync()