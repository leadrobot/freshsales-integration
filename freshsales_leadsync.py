import freshsaleshelper
from sqlalchemy import create_engine, MetaData

postgres_engine = create_engine('postgresql://postgres:BIcA1NM4mvAAJljE@35.225.19.27:5432/betxchange')
meta = MetaData(postgres_engine)
meta.reflect()

users_table = meta.tables['users']
deposits_table = meta.tables['deposits']
bets_table = meta.tables['bets']
withdrawals_table = meta.tables['withdrawals']

def sync():
    freshsaleshelper.Integrator().insert_leads(postgres_engine, users_table, deposits_table, bets_table, withdrawals_table)


if __name__ == '__main__':
	while True:
		sync()