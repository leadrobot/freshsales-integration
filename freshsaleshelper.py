import freshsales
import json
import time
import datetime
from sqlalchemy import update, select, join, desc
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import or_, and_
import csv
import validations

class Integrator():
	def insert_leads(self, postgres_engine, users_table, deposits_table, bets_table, withdrawals_table):
		# Start Temporary criteria
		start_date = datetime.datetime.now() - datetime.timedelta(days=1)
		end_date = datetime.datetime.now()
		# End Temporary criteria

		try:
			
			user_selection = users_table.select().where(or_((and_( 
															users_table.c.customer_id != None,
															users_table.c.created_on_date >=start_date,
															users_table.c.created_on_date <=end_date,
															users_table.c.freshsales_id == None)), 
															(and_(
															users_table.c.customer_id != None,
															users_table.c.modified_on_date >=start_date,
															users_table.c.modified_on_date <=end_date,
															users_table.c.freshsales_id == None)))).order_by(
																desc(users_table.c.created_on_date))
			user_collection = postgres_engine.connect().execute(user_selection)

			if user_collection:
				for user_row in user_collection:
					last_name = user_row.last_name
					if last_name:
						last_name = user_row.last_name
					else:
						last_name = 'Anonymous'

					email_address =  user_row.email_address
					if email_address:
						is_email_valid = validations.is_valid_email(email_address)
						if is_email_valid:
							email_address = user_row.email_address
						else:
							email_address = None

					if user_row.created_on_date:
						created_date = user_row.created_on_date
					else:
						created_date = datetime.datetime.now()

					created_date = datetime.datetime.strftime(created_date, '%Y-%m-%dT%H:%M:%SZ')

					if user_row.lr_directorship:
						if user_row.lr_directorship == True:
							directorship = True
					else:
						directorship = False

					if user_row.lr_homeowner:
						if user_row.lr_homeowner == True:
							homeowner = True
					else:
						homeowner = False


					entrycheck = freshsales.FreshSales('lookup').get_by_customer_id(user_row.customer_id)
					
					if entrycheck:

						if entrycheck.get('leads'):
							if len(entrycheck['leads']['leads']) == 1:
								payload = {
											"lead": {
												"first_name": user_row.first_name,
												"last_name": last_name,
												"mobile_number": user_row.phone_number,
												"email": email_address,
												"address": user_row.physical_address,
												"city": user_row.city,
												"zipcode": user_row.postal_code,
												"custom_field": {
																	"cf_website_username": user_row.username,
																	"cf_customer_id": user_row.customer_id,
																	"cf_date_of_birth": str(user_row.date_of_birth),
																	"cf_id_number": user_row.id_number,
																	"cf_address_line_1": user_row.address_line_1,
																	"cf_address_line_2": user_row.address_line_2,
																	"cf_suburb": user_row.suburb,
																	"cf_province": user_row.province,
																	"cf_postal_code": user_row.postal_code,
																	"cf_referral_code": user_row.referral_code,
																	"cf_language": user_row.language_preference,
																	"cf_fica_status": user_row.fica_status,
																	"cf_user_created_at": str(created_date),
																	"cf_enriched_mobile_number": user_row.lr_cellphone,
																	"cf_enriched_email": user_row.lr_email,
																	"cf_gender": user_row.lr_gender,
																	"cf_marital_status": user_row.lr_marital_status,
																	"cf_population_group": user_row.lr_predicted_ethnicity,
																	"cf_directorship": directorship,
																	"cf_home_owner": homeowner,
																	"cf_predicted_income": user_row.lr_predicted_income,
																	"cf_enriched_address_line1": user_row.lr_physical_address_line1,
																	"cf_enriched_address_line2": user_row.lr_physical_address_line2,
																	"cf_enriched_address_line3": user_row.lr_physical_address_line3,
																	"cf_enriched_address_line4": user_row.lr_physical_address_line4,
																	"cf_enriched_address_suburb": user_row.lr_physical_suburb,
																	"cf_enriched_address_postal_code": user_row.lr_physical_post_code,
																	"cf_enriched_postal_address_line1": user_row.lr_postal_address_line1,
																	"cf_enriched_postal_address_line2": user_row.lr_postal_address_line2,
																	"cf_enriched_postal_address_line3": user_row.lr_postal_address_line3,
																	"cf_enriched_postal_address_line4": user_row.lr_postal_address_line4,
																	"cf_enriched_postal_post_code": user_row.lr_postal_post_code,
																	"cf_enriched_first_name": user_row.lr_first_name,
																	"cf_enriched_last_name": user_row.lr_lastname,
																}
												# "created_at": str(created_date),
												}
											}
								payload = eval(str(payload))
								payload = json.dumps(payload)

								# payload = json.dumps(payload)
								# payload = str(payload).replace("'", '"')
								# time.sleep(1.800)
								freshsales_response = freshsales.FreshSales('leads').update(entrycheck['leads']['leads'][0]['id'], payload)
								if freshsales_response:
									print("Lead: {} {} updated".format(user_row.first_name, last_name))

									update_record = users_table.update().where(users_table.c.username == user_row.username).values(processed = True, 
																																	freshsales_id = freshsales_response.get('lead').get('id'))
									postgres_engine.connect().execute(update_record)
									print("Flagged {} {} to updated".format(user_row.first_name, last_name))
							
							elif len(entrycheck['leads']['leads']) == 0:
								payload = {
											"lead": {
												"first_name": user_row.first_name,
												"last_name": last_name,
												"mobile_number": user_row.phone_number,
												"email": email_address,
												"address": user_row.physical_address,
												"city": user_row.city,
												"zipcode": user_row.postal_code,
												"custom_field": {
																	"cf_website_username": user_row.username,
																	"cf_customer_id": user_row.customer_id,
																	"cf_date_of_birth": str(user_row.date_of_birth),
																	"cf_id_number": user_row.id_number,
																	"cf_address_line_1": user_row.address_line_1,
																	"cf_address_line_2": user_row.address_line_2,
																	"cf_suburb": user_row.suburb,
																	"cf_province": user_row.province,
																	"cf_postal_code": user_row.postal_code,
																	"cf_referral_code": user_row.referral_code,
																	"cf_language": user_row.language_preference,
																	"cf_fica_status": user_row.fica_status,
																	"cf_user_created_at": str(created_date),
																	"cf_enriched_mobile_number": user_row.lr_cellphone,
																	"cf_enriched_email": user_row.lr_email,
																	"cf_gender": user_row.lr_gender,
																	"cf_marital_status": user_row.lr_marital_status,
																	"cf_population_group": user_row.lr_predicted_ethnicity,
																	"cf_directorship": directorship,
																	"cf_home_owner": homeowner,
																	"cf_predicted_income": user_row.lr_predicted_income,
																	"cf_enriched_address_line1": user_row.lr_physical_address_line1,
																	"cf_enriched_address_line2": user_row.lr_physical_address_line2,
																	"cf_enriched_address_line3": user_row.lr_physical_address_line3,
																	"cf_enriched_address_line4": user_row.lr_physical_address_line4,
																	"cf_enriched_address_suburb": user_row.lr_physical_suburb,
																	"cf_enriched_address_postal_code": user_row.lr_physical_post_code,
																	"cf_enriched_postal_address_line1": user_row.lr_postal_address_line1,
																	"cf_enriched_postal_address_line2": user_row.lr_postal_address_line2,
																	"cf_enriched_postal_address_line3": user_row.lr_postal_address_line3,
																	"cf_enriched_postal_address_line4": user_row.lr_postal_address_line4,
																	"cf_enriched_postal_post_code": user_row.lr_postal_post_code,
																	"cf_enriched_first_name": user_row.lr_first_name,
																	"cf_enriched_last_name": user_row.lr_lastname 
																}
												# "created_at": str(created_date),
												}
											}

								payload = eval(str(payload))
								payload = json.dumps(payload)
								
								# payload = json.dumps(payload)
								# payload = str(payload).replace("'", '"')
								# time.sleep(1.800)
								freshsales_response = freshsales.FreshSales('leads').create(payload)
								if freshsales_response:
									print("Lead: {} {} created".format(user_row.first_name, last_name))

									update_record = users_table.update().where(users_table.c.customer_id == 
																			user_row.customer_id).values(processed = True, 
																										freshsales_id = 
																										freshsales_response.get('lead').get('id'))
									postgres_engine.connect().execute(update_record)
									print("Flagged {} {} to updated".format(user_row.first_name, last_name))

						elif entrycheck.get('contacts'):
							if len(entrycheck['contacts']['contacts']) == 1:
								payload = {
											"contact": {
												"first_name": user_row.first_name,
												"last_name": last_name,
												"mobile_number": user_row.phone_number,
												"email": email_address,
												"address": user_row.physical_address,
												"city": user_row.city,
												"zipcode": user_row.postal_code,
												"custom_field": {
																	"cf_website_username": user_row.username,
																	"cf_customer_id": user_row.customer_id,
																	"cf_date_of_birth": str(user_row.date_of_birth),
																	"cf_id_number": user_row.id_number,
																	"cf_address_line_1": user_row.address_line_1,
																	"cf_address_line_2": user_row.address_line_2,
																	"cf_suburb": user_row.suburb,
																	"cf_province": user_row.province,
																	"cf_postal_code": user_row.postal_code,
																	"cf_referral_code": user_row.referral_code,
																	"cf_language": user_row.language_preference,
																	"cf_fica_status": user_row.fica_status,
																	"cf_user_created_at": str(created_date),
																	"cf_enriched_mobile_number": user_row.lr_cellphone,
																	"cf_enriched_email": user_row.lr_email,
																	"cf_gender": user_row.lr_gender,
																	"cf_marital_status": user_row.lr_marital_status,
																	"cf_population_group": user_row.lr_predicted_ethnicity,
																	"cf_directorship": directorship,
																	"cf_home_owner": homeowner,
																	"cf_predicted_income": user_row.lr_predicted_income,
																	"cf_enriched_address_line1": user_row.lr_physical_address_line1,
																	"cf_enriched_address_line2": user_row.lr_physical_address_line2,
																	"cf_enriched_address_line3": user_row.lr_physical_address_line3,
																	"cf_enriched_address_line4": user_row.lr_physical_address_line4,
																	"cf_enriched_address_suburb": user_row.lr_physical_suburb,
																	"cf_enriched_address_postal_code": user_row.lr_physical_post_code,
																	"cf_enriched_postal_address_line1": user_row.lr_postal_address_line1,
																	"cf_enriched_postal_address_line2": user_row.lr_postal_address_line2,
																	"cf_enriched_postal_address_line3": user_row.lr_postal_address_line3,
																	"cf_enriched_postal_address_line4": user_row.lr_postal_address_line4,
																	"cf_enriched_postal_post_code": user_row.lr_postal_post_code,
																	"cf_enriched_first_name": user_row.lr_first_name,
																	"cf_enriched_last_name": user_row.lr_lastname,
																	"cf_deposit_total": str(user_row.total_deposits),
																	"cf_withdrawal_total": str(user_row.total_withdrawals),
																	"cf_account_balance": str(user_row.account_balance)
																}
												# "created_at": str(created_date),
												}
											}
								payload = eval(str(payload))
								payload = json.dumps(payload)

								# payload = json.dumps(payload)
								# payload = str(payload).replace("'", '"')
								# time.sleep(1.800)
								freshsales_response = freshsales.FreshSales('contacts').update(entrycheck['contacts']['contacts'][0]['id'], payload)
								if freshsales_response:
									print("Contact: {} {} updated".format(user_row.first_name, last_name))

									update_record = users_table.update().where(users_table.c.customer_id == 
																							user_row.customer_id).values(
																								processed = True, 
																								freshsales_id = freshsales_response.get('lead').get('id'))
									postgres_engine.connect().execute(update_record)
									print("Flagged {} {} to updated".format(user_row.first_name, last_name))

						elif entrycheck.get('leads') and entrycheck.get('contacts'):
							if len(entrycheck['leads']['leads']) == 0 and len(entrycheck['contacts']['contacts']) == 0:
								payload = {
											"lead": {
												"first_name": user_row.first_name,
												"last_name": last_name,
												"mobile_number": user_row.phone_number,
												"email": email_address,
												"address": user_row.physical_address,
												"city": user_row.city,
												"zipcode": user_row.postal_code,
												"custom_field": {
																	"cf_website_username": user_row.username,
																	"cf_customer_id": user_row.customer_id,
																	"cf_date_of_birth": str(user_row.date_of_birth),
																	"cf_id_number": user_row.id_number,
																	"cf_address_line_1": user_row.address_line_1,
																	"cf_address_line_2": user_row.address_line_2,
																	"cf_suburb": user_row.suburb,
																	"cf_province": user_row.province,
																	"cf_postal_code": user_row.postal_code,
																	"cf_referral_code": user_row.referral_code,
																	"cf_language": user_row.language_preference,
																	"cf_fica_status": user_row.fica_status,
																	"cf_user_created_at": str(created_date),
																	"cf_enriched_mobile_number": user_row.lr_cellphone,
																	"cf_enriched_email": user_row.lr_email,
																	"cf_gender": user_row.lr_gender,
																	"cf_marital_status": user_row.lr_marital_status,
																	"cf_population_group": user_row.lr_predicted_ethnicity,
																	"cf_directorship": directorship,
																	"cf_home_owner": homeowner,
																	"cf_predicted_income": user_row.lr_predicted_income,
																	"cf_enriched_address_line1": user_row.lr_physical_address_line1,
																	"cf_enriched_address_line2": user_row.lr_physical_address_line2,
																	"cf_enriched_address_line3": user_row.lr_physical_address_line3,
																	"cf_enriched_address_line4": user_row.lr_physical_address_line4,
																	"cf_enriched_address_suburb": user_row.lr_physical_suburb,
																	"cf_enriched_address_postal_code": user_row.lr_physical_post_code,
																	"cf_enriched_postal_address_line1": user_row.lr_postal_address_line1,
																	"cf_enriched_postal_address_line2": user_row.lr_postal_address_line2,
																	"cf_enriched_postal_address_line3": user_row.lr_postal_address_line3,
																	"cf_enriched_postal_address_line4": user_row.lr_postal_address_line4,
																	"cf_enriched_postal_post_code": user_row.lr_postal_post_code,
																	"cf_enriched_first_name": user_row.lr_first_name,
																	"cf_enriched_last_name": user_row.lr_lastname 
																}
												# "created_at": str(created_date),
												}
											}

								payload = eval(str(payload))
								payload = json.dumps(payload)
								
								# payload = json.dumps(payload)
								# payload = str(payload).replace("'", '"')
								# time.sleep(1.800)
								freshsales_response = freshsales.FreshSales('leads').create(payload)
								if freshsales_response:
									print("Lead: {} {} created".format(user_row.first_name, last_name))

									update_record = users_table.update().where(users_table.c.username == 
																				user_row.username).values(processed = True, 
																				freshsales_id = freshsales_response.get('lead').get('id'))
									postgres_engine.connect().execute(update_record)
									print("Flagged {} {} to updated".format(user_row.first_name, last_name))

					elif entrycheck == None or len(entrycheck) == 0:
						payload = {
										"lead": {
											"first_name": user_row.first_name,
											"last_name": last_name,
											"mobile_number": user_row.phone_number,
											"email": email_address,
											"address": user_row.physical_address,
											"city": user_row.city,
											"zipcode": user_row.postal_code,
											"custom_field": {
																"cf_website_username": user_row.username,
																"cf_customer_id": user_row.customer_id,
																"cf_date_of_birth": str(user_row.date_of_birth),
																"cf_id_number": user_row.id_number,
																"cf_address_line_1": user_row.address_line_1,
																"cf_address_line_2": user_row.address_line_2,
																"cf_suburb": user_row.suburb,
																"cf_province": user_row.province,
																"cf_postal_code": user_row.postal_code,
																"cf_referral_code": user_row.referral_code,
																"cf_language": user_row.language_preference,
																"cf_fica_status": user_row.fica_status,
																"cf_user_created_at": str(created_date),
																"cf_enriched_mobile_number": user_row.lr_cellphone,
																"cf_enriched_email": user_row.lr_email,
																"cf_gender": user_row.lr_gender,
																"cf_marital_status": user_row.lr_marital_status,
																"cf_population_group": user_row.lr_predicted_ethnicity,
																"cf_directorship": directorship,
																"cf_home_owner": homeowner,
																"cf_predicted_income": user_row.lr_predicted_income,
																"cf_enriched_address_line1": user_row.lr_physical_address_line1,
																"cf_enriched_address_line2": user_row.lr_physical_address_line2,
																"cf_enriched_address_line3": user_row.lr_physical_address_line3,
																"cf_enriched_address_line4": user_row.lr_physical_address_line4,
																"cf_enriched_address_suburb": user_row.lr_physical_suburb,
																"cf_enriched_address_postal_code": user_row.lr_physical_post_code,
																"cf_enriched_postal_address_line1": user_row.lr_postal_address_line1,
																"cf_enriched_postal_address_line2": user_row.lr_postal_address_line2,
																"cf_enriched_postal_address_line3": user_row.lr_postal_address_line3,
																"cf_enriched_postal_address_line4": user_row.lr_postal_address_line4,
																"cf_enriched_postal_post_code": user_row.lr_postal_post_code,
																"cf_enriched_first_name": user_row.lr_first_name,
																"cf_enriched_last_name": user_row.lr_lastname 
																
															}
											# "created_at": str(created_date),
											}
										}

						payload = eval(str(payload))
						payload = json.dumps(payload)
						
						# payload = json.dumps(payload)
						# payload = str(payload).replace("'", '"')
						# time.sleep(1.800)
						freshsales_response = freshsales.FreshSales('leads').create(payload)
						if freshsales_response:
							print("Lead: {} {} created".format(user_row.first_name, last_name))

							update_record = users_table.update().where(users_table.c.username == user_row.username).values(processed = True, 
																		freshsales_id = freshsales_response.get('lead').get('id'))
							postgres_engine.connect().execute(update_record)
							print("Flagged {} {} to updated".format(user_row.first_name, last_name))
		except Exception as ex:
			print(ex)

	def insert_bets(self, postgres_engine, bets_table, bet_lines_table):
		try:
			
			bet_selection = bets_table.select().where(and_(bets_table.c.freshsales_id == None, bets_table.c.customer_id != None,
															bets_table.c.created_on_date >= 
															(datetime.date.today() - datetime.timedelta(days=1)),
															bets_table.c.created_on_date <= datetime.date.today(),
															bets_table.c.stake_won > 0,
															bets_table.c.freshsales_id == None)
															).order_by(desc(bets_table.c.created_on_date))
			bets_collection = postgres_engine.connect().execute(bet_selection)

			if bets_collection and bets_collection.rowcount > 0:
				for bet_row in bets_collection:
				
					entrycheck = freshsales.FreshSales('lookup').get_by_customer_id(bet_row.customer_id)

					if len(entrycheck.get('leads').get('leads')) == 1:
						# Convert Lead into contact
						payload = {
											"lead": {
												"last_name": entrycheck['leads']['leads'][0]['last_name'],
												"company": {
																"name": "{} {}".format(entrycheck['leads']['leads'][0]['first_name'], 
																						entrycheck['leads']['leads'][0]['last_name'])
															}
											}
									}
						payload = eval(str(payload))
						payload = json.dumps(payload)
						# payload = json.dumps(payload)
						# payload = str(payload).replace("'", '"')
						time.sleep(1.800)
						freshsales_response = freshsales.FreshSales('leads').convert(entrycheck['leads']['leads'][0]['id'], payload)
						print("Lead: {} converted to contact".format(entrycheck['leads']['leads'][0]['id']))
						
						payload = {
									"deal": {
										"name": bet_row.transaction_id,
										"amount": str(bet_row.stake_won),
										"deal_type_id": 4000054460,
										"custom_field": {
											"cf_stake_won": str(bet_row.stake_won),
											"cf_customer_id": bet_row.customer_id,
											"cf_transaction_created_at": str(bet_row.created_on_date)
										},
										"contacts_added_list": [
											freshsales_response.get('contact').get('id')
										]
									}
								}
								
						payload = eval(str(payload))
						payload = json.dumps(payload)
						time.sleep(1.800)
						freshsales_response = freshsales.FreshSales('deals').create(payload)
						print("Bet: {} created".format(bet_row.transaction_id))

					elif len(entrycheck.get('contacts').get('contacts')) == 1:
						# # Update contact and assign transaction to him
						# payload = {
						# 			"contact":
						# 						{	
						# 							"first_name": entrycheck['contacts']['contacts'][0]['first_name'],
						# 							"last_name":entrycheck['contacts']['contacts'][0]['last_name']
						# 						}
						# 				}
						# payload = eval(str(payload))
						# payload = json.dumps(payload)
						# time.sleep(1.800)
						# freshsales_response = freshsales.FreshSales('contacts').update(entrycheck['contacts']['contacts'][0]['id'], payload)

						payload = {
									"deal": {
										"name": bet_row.transaction_id,
										"amount": str(bet_row.stake_won),
										"deal_type_id": 4000054460,
										"custom_field": {
											"cf_stake_won": str(bet_row.stake_won),
											"cf_customer_id": bet_row.customer_id,
											"cf_transaction_created_at": str(bet_row.created_on_date)
										},
										"contacts_added_list": [
											entrycheck['contacts']['contacts'][0]['id']	
										]
									}
								}
						payload = eval(str(payload))
						payload = json.dumps(payload)
						time.sleep(1.800)
						freshsales_response = freshsales.FreshSales('deals').create(payload)
						print("Bet: {} created".format(bet_row.transaction_id))

					else:
						freshsales_response = None
					
					if freshsales_response:
						if freshsales_response.get('deal').get('id'):
							update_record = bets_table.update().where(bets_table.c.id == bet_row.id).values(processed = True, 
																			freshsales_id = freshsales_response.get('deal').get('id'))
							postgres_engine.connect().execute(update_record)
							print("Flagged {} to updated".format(bet_row.transaction_id))
					
		except Exception as ex:
			print(ex)

	def insert_withdrawals(self, postgres_engine, withdrawals_table):
		# Start Temporary criteria
		start_date = datetime.datetime.now() - datetime.timedelta(days=1)
		end_date = datetime.datetime.now()
		# End Temporary criteria	
		try:
			withdrawals_selection = withdrawals_table.select().where(and_(withdrawals_table.c.customer_id != None, 
																			withdrawals_table.c.freshsales_id == None, 
																			withdrawals_table.c.transaction_id != None,
																			withdrawals_table.c.created_on_date >= start_date,
																			withdrawals_table.c.created_on_date <= end_date,
																			withdrawals_table.c.freshsales_id == None)
																			).order_by(
																	desc(withdrawals_table.c.created_on_date))
			withdrawals_collection = postgres_engine.connect().execute(withdrawals_selection)
			
			if withdrawals_collection and withdrawals_collection.rowcount > 0:
				for withdrawal_row in withdrawals_collection:
					entrycheck = freshsales.FreshSales('lookup').get_by_customer_id(withdrawal_row.customer_id)

					if len(entrycheck.get('leads').get('leads')) == 1:
						# Convert Lead into contact
						payload = {
											"lead": {
												"last_name": entrycheck['leads']['leads'][0]['last_name'],
												"company": {
																"name": "{} {}".format(entrycheck['leads']['leads'][0]['first_name'], 
																						entrycheck['leads']['leads'][0]['last_name'])
															}
											}
									}
						payload = eval(str(payload))
						payload = json.dumps(payload)
						# payload = json.dumps(payload)
						# payload = str(payload).replace("'", '"')
						time.sleep(1.800)
						freshsales_response = freshsales.FreshSales('leads').convert(entrycheck['leads']['leads'][0]['id'], payload)
						print("Lead: {} converted to contact".format(entrycheck['leads']['leads'][0]['id']))
						
						payload = {"deal": 
									{	
										"name": withdrawal_row.transaction_id,
										"amount": str(withdrawal_row.requested_amount),
										"deal_type_id": 4000054459,
										"custom_field": {
															"cf_username": withdrawal_row.username,
															"cf_customer_id": withdrawal_row.customer_id,
															"cf_created_on_date": str(withdrawal_row.created_on_date),
															"cf_requested_amount": str(withdrawal_row.requested_amount),
															"cf_approved_amount": str(withdrawal_row.approved_amount),
															"cf_withdrawal_type": withdrawal_row.withdrawal_type,
															"cf_shop": withdrawal_row.shop,
															"cf_location": withdrawal_row.location,
															"cf_status": withdrawal_row.status,
															"cf_reason": withdrawal_row.reason,
															"cf_transaction_created_at": str(withdrawal_row.created_on_date)
														},
										"contacts_added_list": [freshsales_response.get('contact').get('id')]
									}
							}
						payload = eval(str(payload))
						payload = json.dumps(payload)
						time.sleep(1.800)
						freshsales_response = freshsales.FreshSales('deals').create(payload)
						print("Withdrawal: {} created".format(withdrawal_row.transaction_id))

					elif len(entrycheck.get('contacts').get('contacts')) == 1:
						# Update contact and assign transaction to him
						payload = {
									"contact":
												{
													"last_name": entrycheck.get('contacts').get('contacts')[0]['last_name']
												},
												"custom_field": {
																	"cf_customer_id": withdrawal_row.customer_id
																}
										}
						payload = eval(str(payload))
						payload = json.dumps(payload)
						time.sleep(1.800)
						freshsales_response = freshsales.FreshSales('contacts').update(entrycheck['contacts']['contacts'][0]['id'], payload)

						payload = {"deal": 
									{	
										"name": withdrawal_row.transaction_id,
										"amount": str(withdrawal_row.requested_amount),
										"deal_type_id": 4000054459,
										"custom_field": {
															"cf_username": withdrawal_row.username,
															"cf_customer_id": withdrawal_row.customer_id,
															"cf_created_on_date": str(withdrawal_row.created_on_date),
															"cf_requested_amount": str(withdrawal_row.requested_amount),
															"cf_approved_amount": str(withdrawal_row.approved_amount),
															"cf_withdrawal_type": withdrawal_row.withdrawal_type,
															"cf_shop": withdrawal_row.shop,
															"cf_location": withdrawal_row.location,
															"cf_status": withdrawal_row.status,
															"cf_reason": withdrawal_row.reason,
															"cf_transaction_created_at": str(withdrawal_row.created_on_date)
														},
										"contacts_added_list": [freshsales_response.get('contact').get('id')]
									}
							}
						payload = eval(str(payload))
						payload = json.dumps(payload)
						time.sleep(1.800)
						freshsales_response = freshsales.FreshSales('deals').create(payload)
						print("Withdrawal: {} created".format(withdrawal_row.transaction_id))
					else:
						freshsales_response = None
					
					if freshsales_response:
						update_record = withdrawals_table.update().where(withdrawals_table.c.id == withdrawal_row.id).values(processed = True, 
																			freshsales_id = freshsales_response.get('deal').get('id'))
						postgres_engine.connect().execute(update_record)
						print("Flagged {} to updated".format(withdrawal_row.transaction_id))
		except Exception as ex:
			print(ex)

	def insert_deposits(self, postgres_engine, deposits_table, users_table):
		try:
			join_object = join(deposits_table, users_table, deposits_table.c.customer_id == users_table.c.customer_id)

			selection = select([deposits_table]).select_from(join_object).where(and_(deposits_table.c.freshsales_id == None, 
																						users_table.c.freshsales_id != None,
																						deposits_table.c.created_on_date >= 
																						(datetime.date.today() - datetime.timedelta(days=1)),
																						deposits_table.c.created_on_date <= 
																						datetime.date.today())).order_by(desc(
																							deposits_table.c.created_on_date))
			
			deposits_collection = postgres_engine.connect().execute(selection)

			# deposits_selection = deposits_table.select().where(or_(deposits_table.c.freshsales_id == 0, deposits_table.c.freshsales_id == None))
			# deposits_collection = postgres_engine.connect().execute(deposits_selection)

			if deposits_collection and deposits_collection.rowcount > 0:
				for deposit_row in deposits_collection:

					# Don't create a deposit if the username does not exist in freshsales.
					# Should you find a username, check if it's a Contact or Lead.
					# If it's a Lead, insert the deal/deposit and convert the Lead into a Contact.
					# If it's a Contact, update the cf_last_deposit_date and update the amount.


					entrycheck = freshsales.FreshSales('lookup').get_by_customer_id(deposit_row.customer_id)
					if entrycheck:
						if entrycheck.get('leads'):

							if len(entrycheck['leads']['leads']) == 1:
								payload = {
											"lead": {
												"last_name": entrycheck['leads']['leads'][0]['last_name'],
												"company": {
																"name": "{} {}".format(entrycheck['leads']['leads'][0]['first_name'], 
																						entrycheck['leads']['leads'][0]['last_name'])
															},
												"custom_field": {
																		# "cf_deposit_count": 1,
																		"cf_deposit_total": str(deposit_row.approved_amount),
																		"cf_first_deposit_date": str(deposit_row.created_on_date),
																		"cf_last_deposit_date": str(deposit_row.created_on_date),
																		"cf_days_since_last_deposit": 0
																}
												}
											}
								payload = eval(str(payload))
								payload = json.dumps(payload)
								# payload = json.dumps(payload)
								# payload = str(payload).replace("'", '"')
								time.sleep(1.800)

								freshsales_response = freshsales.FreshSales('leads').convert(entrycheck['leads']['leads'][0]['id'], payload)
								print("Lead: {} converted to contact".format(entrycheck['leads']['leads'][0]['id']))

								payload = {"deal": 
												{	
													"name": deposit_row.transaction_id,
													"amount": str(deposit_row.requested_amount), #approved_amount
													"deal_type_id": 4000054458,
													"custom_field": {
																		"cf_username": deposit_row.username,
																		"cf_customer_id": deposit_row.customer_id,
																		"cf_created_on_date": str(deposit_row.created_on_date),
																		"cf_method_type": deposit_row.method_type,
																		"cf_payment_method_id": deposit_row.payment_method_id,
																		"cf_bank_transfer_beneficiary_name": 
																		deposit_row.bank_transfer_beneficiary_name,
																		"cf_requested_amount": str(deposit_row.requested_amount),
																		"cf_approved_amount": str(deposit_row.approved_amount),
																		"cf_bank_transfer_bank_account_number": 
																		deposit_row.bank_transfer_bank_account_number,
																		"cf_bank_transfer_bank_name": deposit_row.bank_transfer_bank_name,
																		"cf_bank_transfer_swift_code": deposit_row.bank_transfer_swift_code,
																		"cf_currency_id": deposit_row.currency_id,
																		"cf_bank_transfer_city": deposit_row.bank_transfer_city,
																		"cf_bank_transfer_country": deposit_row.bank_transfer_country,
																		"cf_aditional_instructions": deposit_row.additional_instructions,
																		"cf_status": deposit_row.status,
																		"cf_transaction_created_at": str(deposit_row.created_on_date)
																	},
													"contacts_added_list":[freshsales_response.get('contact').get('id')]
												}
										}
								payload = eval(str(payload))
								payload = json.dumps(payload)
								# payload = json.dumps(payload)
								# payload = str(payload).replace("'", '"')
								time.sleep(1.800)
								freshsales.FreshSales('deals').create(payload)
								print("Deposits: {} created".format(deposit_row.transaction_id))

								update_record = deposits_table.update().where(deposits_table.c.id == deposit_row.id).values(processed = True, 
																				freshsales_id = freshsales_response.get('contact').get('id'))
								postgres_engine.connect().execute(update_record)
								print("Flagged {} to updated".format(deposit_row.transaction_id))

							elif len(entrycheck['leads']['leads']) == 2:
								payload = {
											"lead": {
												"last_name": entrycheck['leads']['leads'][1]['last_name'],
												"company": {
																"name": "{} {}".format(entrycheck['leads']['leads'][1]['first_name'], 
																						entrycheck['leads']['leads'][1]['last_name'])
															},
												"custom_field": {
																		"cf_deposit_count": 1,
																		"cf_deposit_total": str(deposit_row.approved_amount),
																		"cf_first_deposit_date": str(deposit_row.created_on_date),
																		"cf_last_deposit_date": str(deposit_row.created_on_date),
																		"cf_days_since_last_deposit": 0
																}
												}
											}
								payload = eval(str(payload))
								payload = json.dumps(payload)
								# payload = json.dumps(payload)
								# payload = str(payload).replace("'", '"')
								time.sleep(1.800)

								freshsales_response = freshsales.FreshSales('leads').convert(entrycheck['leads']['leads'][1]['id'], payload)
								print("Lead: {} converted to contact".format(entrycheck['leads']['leads'][1]['id']))

								payload = {"deal": 
												{	
													"name": deposit_row.transaction_id,
													"amount": str(deposit_row.requested_amount),
													"deal_type_id": 4000054458,
													"custom_field": {
																		"cf_username": deposit_row.username,
																		"cf_customer_id": deposit_row.customer_id,
																		"cf_created_on_date": str(deposit_row.created_on_date),
																		"cf_method_type": deposit_row.method_type,
																		"cf_payment_method_id": deposit_row.payment_method_id,
																		"cf_bank_transfer_beneficiary_name": 
																		deposit_row.bank_transfer_beneficiary_name,
																		"cf_requested_amount": str(deposit_row.requested_amount),
																		"cf_approved_amount": str(deposit_row.approved_amount),
																		"cf_bank_transfer_bank_account_number": 
																		deposit_row.bank_transfer_bank_account_number,
																		"cf_bank_transfer_bank_name": deposit_row.bank_transfer_bank_name,
																		"cf_bank_transfer_swift_code": deposit_row.bank_transfer_swift_code,
																		"cf_currency_id": deposit_row.currency_id,
																		"cf_bank_transfer_city": deposit_row.bank_transfer_city,
																		"cf_bank_transfer_country": deposit_row.bank_transfer_country,
																		"cf_aditional_instructions": deposit_row.additional_instructions,
																		"cf_status": deposit_row.status,
																		"cf_transaction_created_at": str(deposit_row.created_on_date)
																	},
													"contacts_added_list":[freshsales_response.get('contact').get('id')]
												}
										}
								payload = eval(str(payload))
								payload = json.dumps(payload)
								# payload = json.dumps(payload)
								# payload = str(payload).replace("'", '"')
								time.sleep(1.800)
								freshsales.FreshSales('deals').create(payload)
								print("Deposits: {} created".format(deposit_row.transaction_id))

								update_record = deposits_table.update().where(deposits_table.c.id == 
																				deposit_row.id).values(processed = True, 
																				freshsales_id = freshsales_response.get('contact').get('id'))
								postgres_engine.connect().execute(update_record)
								print("Flagged {} to updated".format(deposit_row.transaction_id))

						elif entrycheck.get('contacts'):
							if len(entrycheck['contacts']['contacts']) == 1:
								payload = {
											"deal": 
											{	
												"name": deposit_row.transaction_id,
												"amount": str(deposit_row.requested_amount),
												"deal_type_id": 4000054458,
												"custom_field": {
																	"cf_username": deposit_row.username,
																	"cf_customer_id": deposit_row.customer_id,
																	"cf_created_on_date": str(deposit_row.created_on_date),
																	"cf_method_type": deposit_row.method_type,
																	"cf_payment_method_id": deposit_row.payment_method_id,
																	"cf_bank_transfer_beneficiary_name": 
																	deposit_row.bank_transfer_beneficiary_name,
																	"cf_requested_amount": str(deposit_row.requested_amount),
																	"cf_approved_amount": str(deposit_row.approved_amount),
																	"cf_bank_transfer_bank_account_number": 
																	deposit_row.bank_transfer_bank_account_number,
																	"cf_bank_transfer_bank_name": deposit_row.bank_transfer_bank_name,
																	"cf_bank_transfer_swift_code": deposit_row.bank_transfer_swift_code,
																	"cf_currency_id": deposit_row.currency_id,
																	"cf_bank_transfer_city": deposit_row.bank_transfer_city,
																	"cf_bank_transfer_country": deposit_row.bank_transfer_country,
																	"cf_aditional_instructions": deposit_row.additional_instructions,
																	"cf_status": deposit_row.status,
																	"cf_transaction_created_at": str(deposit_row.created_on_date)
																},
												"contacts_added_list":[freshsales_response.get('contact').get('id')]
											}

										}		
								payload = eval(str(payload))
								payload = json.dumps(payload)
								# payload = json.dumps(payload)
								# payload = str(payload).replace("'", '"')
								time.sleep(1.800)
								freshsales.FreshSales('deals').create(payload)
								print("Deposits: {} created".format(deposit_row.transaction_id))

								payload = {
											"contact": {
												"custom_field": {
																		"cf_deposit_count": 1,
																		"cf_deposit_total": str(deposit_row.approved_amount),
																		"cf_first_deposit_date": str(deposit_row.created_on_date),
																		"cf_last_deposit_date": str(deposit_row.created_on_date),
																		"cf_days_since_last_deposit": 0
																}
												}
											}
								payload = eval(str(payload))
								payload = json.dumps(payload)
								# payload = json.dumps(payload)
								# payload = str(payload).replace("'", '"')
								time.sleep(1.800)

								freshsales_response = freshsales.FreshSales('contacts').update(entrycheck['contacts']['contacts'][0]['id'], payload)
								print("Contact: {} updated".format(entrycheck['contacts']['contacts'][0]['id']))

								update_record = deposits_table.update().where(deposits_table.c.id == 
																				deposit_row.id).values(processed = True, 
																				freshsales_id = freshsales_response.get('contact').get('id'))
								postgres_engine.connect().execute(update_record)
								print("Flagged {} to updated".format(deposit_row.transaction_id))	

							elif len(entrycheck['contacts']['contacts']) == 2:
								payload = {
											"deal": 
											{	
												"name": deposit_row.transaction_id,
												"amount": str(deposit_row.requested_amount),
												"deal_type_id": 4000054458,
												"custom_field": {
																	"cf_username": deposit_row.username,
																	"cf_customer_id": deposit_row.customer_id,
																	"cf_created_on_date": str(deposit_row.created_on_date),
																	"cf_method_type": deposit_row.method_type,
																	"cf_payment_method_id": deposit_row.payment_method_id,
																	"cf_bank_transfer_beneficiary_name": deposit_row.bank_transfer_beneficiary_name,
																	"cf_requested_amount": str(deposit_row.requested_amount),
																	"cf_approved_amount": str(deposit_row.approved_amount),
																	"cf_bank_transfer_bank_account_number": deposit_row.bank_transfer_bank_account_number,
																	"cf_bank_transfer_bank_name": deposit_row.bank_transfer_bank_name,
																	"cf_bank_transfer_swift_code": deposit_row.bank_transfer_swift_code,
																	"cf_currency_id": deposit_row.currency_id,
																	"cf_bank_transfer_city": deposit_row.bank_transfer_city,
																	"cf_bank_transfer_country": deposit_row.bank_transfer_country,
																	"cf_aditional_instructions": deposit_row.additional_instructions,
																	"cf_status": deposit_row.status,
																	"cf_transaction_created_at": str(deposit_row.created_on_date)
																},
												"contacts_added_list":[freshsales_response.get('contact').get('id')]
											}

										}		
								payload = eval(str(payload))
								payload = json.dumps(payload)
								# payload = json.dumps(payload)
								# payload = str(payload).replace("'", '"')
								time.sleep(1.800)
								freshsales.FreshSales('deals').create(payload)
								print("Deposits: {} created".format(deposit_row.transaction_id))

								payload = {
											"contact": {
												"custom_field": {
																		"cf_deposit_count": 1,
																		"cf_deposit_total": str(deposit_row.approved_amount),
																		"cf_first_deposit_date": str(deposit_row.created_on_date),
																		"cf_last_deposit_date": str(deposit_row.created_on_date),
																		"cf_days_since_last_deposit": 0
																}
												}
											}
								payload = eval(str(payload))
								payload = json.dumps(payload)
								# payload = json.dumps(payload)
								# payload = str(payload).replace("'", '"')
								time.sleep(1.800)

								freshsales_response = freshsales.FreshSales('contacts').update(entrycheck['contacts']['contacts'][1]['id'], payload)
								print("Contact: {} updated".format(entrycheck['contacts']['contacts'][1]['id']))

								update_record = deposits_table.update().where(deposits_table.c.id == deposit_row.id).values(processed = True, freshsales_id = freshsales_response.get('contact').get('id'))
								postgres_engine.connect().execute(update_record)
								print("Flagged {} to updated".format(deposit_row.transaction_id))
						   
		except Exception as ex:
			print(ex)