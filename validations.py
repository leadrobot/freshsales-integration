import re
import csv
import smtplib
import dns.resolver
from dns.resolver import NXDOMAIN

from_address = 'dan@mood.co.za'

def is_valid_idnumber(idnumber):

	if len(idnumber) !=13:
		return False

	if int(idnumber[2:4]) > 12:
		return False

	if int(idnumber[4:6]) > 31:
		return False
	
	check = int(idnumber[-1])
	number = idnumber[:12]
	odd_sum = 0
	even_digits = ''
	count = 0
	for digit in number:
		if count%2 == 0:
			odd_sum += int(digit)
		else:
			even_digits = even_digits + digit
		count+=1
	even_product = str(int(even_digits) * 2)
	even_sum = 0
	for digit in even_product:
		even_sum += int(digit)
	calc = odd_sum + even_sum
	calc = 10 - int(str(calc)[-1])
	if calc == 10:
		calc = 0

	if calc != check:
		return False

	return True

def is_valid_phone(number):
	number = re.sub("[^0-9]", "", number)
	if number == '':
		return False
	elif number[0:2] == '27':
		if len(number) == 11:
			return True
	elif number[0] == '0':
		if len(number) == 10:
			return True
	return False

def is_valid_email(email):
	if re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$', email):
		return True
	return False

# def is_valid_email(email):
# 	if re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$', email): #if re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', email):--Orginal
# 		try:
# 			split_email_address = email.split('@')
# 			email_domain = str(split_email_address[1])

# 			records = dns.resolver.query(email_domain, 'MX')
# 			mxRecord = records[0].exchange
# 			mxRecord = str(mxRecord)

# 			server = smtplib.SMTP()
# 			server.set_debuglevel(0)

# 			server.connect(mxRecord)
# 			server.helo(server.local_hostname)
# 			server.mail(from_address)
# 			code = server.rcpt(str(email))
# 			server.quit()
			
# 			if code[0] == 250:
# 				return True
# 		except NXDOMAIN:
# 			return False
# 	return False

def is_valid_postal_code(postal_code):
	postal_code = re.sub("[^0-9]", "", postal_code)
	with open('API/resources/postalcodes.csv') as csvfile:
		postalcodes = csv.reader(csvfile)
		for code in postalcodes:
			if postal_code == code[3] or postal_code == code[4]:
				return True
	return False


#Tests
if __name__ == '__main__':
	valid_email = 'dan@mood.co.za'
	invalid_email = 'dan@mood'
	valid_id = '8107255026085'
	invalid_id = '8107255026086'
	valid_phone = '0718368936'
	invalid_phone = '071836893'
	valid_postcode = '2196'
	invalid_postcode = '9999'
	

	print ('Should all be True:')
	print (is_valid_email(valid_email))
	print (is_valid_phone(valid_phone))
	print (is_valid_idnumber(valid_id))
	print (is_valid_postal_code(valid_postcode))
	print ('Should all be False:')
	print (is_valid_email(invalid_email))
	print (is_valid_phone(invalid_phone))
	print (is_valid_idnumber(invalid_id))
	print (is_valid_postal_code(invalid_postcode))