import freshsaleshelper
from sqlalchemy import create_engine, MetaData

postgres_engine = create_engine('postgresql://postgres:BIcA1NM4mvAAJljE@35.225.19.27:5432/betxchange')
meta = MetaData(postgres_engine)
meta.reflect()

withdrawals_table = meta.tables['withdrawals']

def sync():
    freshsaleshelper.Integrator().insert_withdrawals(postgres_engine, withdrawals_table)

if __name__ == '__main__':
	while True:
		sync()