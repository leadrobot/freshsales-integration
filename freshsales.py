import requests
from pprint import pprint

api_key = 'YPmnWe2GGuO1crKmcFoa3A'
base_url = 'https://betxchange.freshsales.io/api/'

headers = {
    'Authorization': 'Token token=' + api_key,
    'Content-Type': 'application/json'
}

class FreshSales:
    def __init__(self, entity):
        self.entity = entity
        print ('started')
        print (self.entity)

    def get_by_id(self, entity_id):
        response = requests.get(base_url + self.entity + '/{}'.format(entity_id), headers=headers)
        if response.status_code == 200: 
            return response.json()


    ##### Get more customization ideas from @Daniel Solomon
    def get_by_username(self, username):
        response = requests.get(base_url + self.entity + '/?q={}&f=cf_website_username&entities=lead,contact'.format(username), headers=headers)
        if response.status_code == 200:
            return response.json()

    def get_by_customer_id(self, customer_id):
        response = requests.get(base_url + self.entity + '/?q={}&f=cf_customer_id&entities=lead,contact'.format(customer_id), headers=headers)
        if response.status_code == 200:
            return response.json()

    def get_transaction_by_transaction_id(self, transaction_id):
        response = requests.get(base_url + self.entity + '/?q={}&f=name&entities=deal'.format(transaction_id), headers=headers)
        if response.status_code == 200:
            return response.json()

    def create(self, data):
        response = requests.post(base_url + self.entity, headers=headers, json=data)
        print (response)
        if response.status_code == 200: 
            return response.json()

    def update(self, entity_id, data):
        response = requests.put(base_url + self.entity + '/{}'.format(entity_id), headers=headers, json=data)
        if response.status_code == 200: 
            return response.json()

    def convert(self, entity_id, data):
        response = requests.post(base_url + self.entity + '/{}/convert'.format(entity_id), headers=headers, json=data)
        if response.status_code == 200:
            return response.json()


if __name__ == '__main__':

    lead_data = {
        "lead":{
            'first_name':'Test', 
            'last_name':'123'
            }
        }

    response = FreshSales('leads').create(lead_data)
    pprint (response)